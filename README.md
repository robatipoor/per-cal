# persian calendar for rust program
### unstable lib work in progress

[![Crates.io](https://img.shields.io/crates/v/per_cal.svg?style=plastic)](http://crates.io/crates/per_cal)
[![Build Status](https://travis-ci.org/robatipoor/per-cal.svg?branch=master)](https://travis-ci.org/robatipoor/per-cal)
[![Build status](https://ci.appveyor.com/api/projects/status/mmbn9g0ux1xf0h23/branch/master?svg=true)](https://ci.appveyor.com/project/robatipoor/per-cal/branch/master)
[![License: MIT](https://img.shields.io/badge/license-MIT-blue.svg)](LICENSE)


## Getting started
Cargo.toml 
```toml

[dependencies]
per_cal="*"

```
main.rs 
```rust

use per_cal::*;

```

2- Convert Gregorian calendar to Persian calendar.

```rust
    // Create a new instance of time.Time
    let loc = chrono::Local::now();
    let t: Time = Time::new(loc);
    // Get the date in Persian calendar
    println!("{:?}", t);
```

3- Convert Persian calendar to Gregorian calendar.

```rust
    // Create a new instance of ptime.Time
    let ptime = Time::date_time(1394, Month::Mehr, 2, 12, 59, 59, 0);
    // Get the date in Gregorian calendar
    println!("{:?}", ptime.to_gregorian()); // output: 2015 September 24
```

4- Get the current time.

```rust
    // Get a new instance of ptime.Time representing the current time
    let pt = Time::now();

    // Get year, month, day
    println!("{:?}", pt.date()); 
    println!("{}{:?}{}", pt.year(), pt.month(), pt.day()); 

    // Get hour, minute, second
    println!("{:?}", pt.clock()); 
    println!("{} {} {}", pt.hour(), pt.minute(), pt.second());

    // Get Unix timestamp (the number of seconds since January 1, 1970 UTC)
    println!("{:?}", pt.unix());

    // Get yesterday, today and tomorrow
    println!("{:?}", pt.yesterday().week_day());
    println!("{:?}", pt.week_day()); 
    println!("{:?}", pt.tomorrow().week_day());

    // Get First and last day of week
    println!("{:?}", pt.first_weekday().date()); 
    println!("{:?}", pt.last_weekday().date());

    // Get First and last day of month
    println!("{:?}", pt.first_monthday().week_day());
    println!("{:?}", pt.last_monthday().week_day());

    // Get First and last day of year
    println!("{:?}", pt.first_yearday().week_day()); 
    println!("{:?}", pt.last_yearday().week_day());

    // Get the week of month
    println!("{:?}", pt.month_week()); 

    // Get the week of year
    println!("{:?}", pt.year_week());

    // Get the number of remaining weeks of the year
    println!("{:?}", pt.remain_year_week());
```

5- Format the time.

```rust
    // Get a new instance of ptime.Time representing the current time
    let pt = Time::now();
    pt.format("yyyy/MM/dd E hh:mm:ss a")

// yyyy, yyy, y     year (e.g. 1394)
// yy               2-digits representation of year (e.g. 94)
// MMM              the Persian name of month (e.g. فروردین)
// MMI              the Dari name of month (e.g. حمل)
// MM               2-digits representation of month (e.g. 01)
// M                month (e.g. 1)
// rw               remaining weeks of year
// w                week of year
// W                week of month
// RD               remaining days of year
// D                day of year
// rd               remaining days of month
// dd               2-digits representation of day (e.g. 01)
// d                day (e.g. 1)
// E                the Persian name of weekday (e.g. شنبه)
// e                the Persian short name of weekday (e.g. ش)
// A                the Persian name of 12-Hour marker (e.g. قبل از ظهر)
// a                the Persian short name of 12-Hour marker (e.g. ق.ظ)
// HH               2-digits representation of hour [00-23]
// H                hour [0-23]
// kk               2-digits representation of hour [01-24]
// k                hour [1-24]
// hh               2-digits representation of hour [01-12]
// h                hour [1-12]
// KK               2-digits representation of hour [00-11]
// K                hour [0-11]
// mm               2-digits representation of minute [00-59]
// m                minute [0-59]
// ss               2-digits representation of seconds [00-59]
// s                seconds [0-59]
// ns               nanoseconds
// S                3-digits representation of milliseconds (e.g. 001)
// z                the name of location
// Z                zone offset (e.g. +03:30)
```

## Documentation

