use per_cal::*;

fn main() {
    // Create a new instance of time.Time
    let loc = chrono::Local::now();
    let t: Time = Time::new(loc);
    // Get the date in Persian calendar
    println!("{:?}", t);
}
