//! Persian Calendar v 0.1.0
//! Please visit https://github.com/robatipoor/per-cal for more information.
//!
//! Copyright (c) 2019 mahdi robatipoor
//! This source code is licensed under MIT license that can be found in the LICENSE file.
//! Package ptime provides functionality for implementation of Persian (Solar Hijri) Calendar.

#![allow(dead_code)]
use chrono::prelude::*;
use strum_macros::ToString;

#[cfg(test)]
mod tests;

// A Month specifies a month of the year starting from Farvardin = 1.
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone, Copy, ToString)]
pub enum Month {
    Farvardin = 1,
    Ordibehesht,
    Khordad,
    Tir,
    Mordad,
    Shahrivar,
    Mehr,
    Aban,
    Azar,
    Dey,
    Bahman,
    Esfand,
}
impl Month {
    pub fn farsi_string(&self) -> String {
        let r = match self {
            Month::Farvardin => MONTH_FARSI[0],
            Month::Ordibehesht => MONTH_FARSI[1],
            Month::Khordad => MONTH_FARSI[2],
            Month::Tir => MONTH_FARSI[3],
            Month::Mordad => MONTH_FARSI[4],
            Month::Shahrivar => MONTH_FARSI[5],
            Month::Mehr => MONTH_FARSI[6],
            Month::Aban => MONTH_FARSI[7],
            Month::Azar => MONTH_FARSI[8],
            Month::Dey => MONTH_FARSI[9],
            Month::Bahman => MONTH_FARSI[10],
            Month::Esfand => MONTH_FARSI[11],
        };
        return r.to_owned();
    }
    fn month_number(i: i32) -> Month {
        if i == 1 {
            return Month::Farvardin;
        } else if i == 2 {
            return Month::Ordibehesht;
        } else if i == 3 {
            return Month::Khordad;
        } else if i == 4 {
            return Month::Tir;
        } else if i == 5 {
            return Month::Mordad;
        } else if i == 6 {
            return Month::Shahrivar;
        } else if i == 7 {
            return Month::Mehr;
        } else if i == 8 {
            return Month::Aban;
        } else if i == 9 {
            return Month::Azar;
        } else if i == 10 {
            return Month::Dey;
        } else if i == 11 {
            return Month::Bahman;
        } else if i == 12 {
            return Month::Esfand;
        } else {
            panic!("invalid input");
        }
    }
}
// List of Dari months in Persian calendar.
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone, Copy, ToString)]
pub enum MonthDari {
    Hamal = 1,
    Sur,
    Jauza,
    Saratan,
    Asad,
    Sonboleh,
    Mizan,
    Aqrab,
    Qos,
    Jady,
    Dolv,
    Hut,
}
impl MonthDari {
    pub fn farsi_string(&self) -> String {
        let r = match self {
            MonthDari::Hamal => MONTH_DARI[0],
            MonthDari::Sur => MONTH_DARI[1],
            MonthDari::Jauza => MONTH_DARI[2],
            MonthDari::Saratan => MONTH_DARI[3],
            MonthDari::Asad => MONTH_DARI[4],
            MonthDari::Sonboleh => MONTH_DARI[5],
            MonthDari::Mizan => MONTH_DARI[6],
            MonthDari::Aqrab => MONTH_DARI[7],
            MonthDari::Qos => MONTH_DARI[8],
            MonthDari::Jady => MONTH_DARI[9],
            MonthDari::Dolv => MONTH_DARI[10],
            MonthDari::Hut => MONTH_DARI[11],
        };
        return r.to_owned();
    }

    fn month_number(i: i32) -> MonthDari {
        if i == 1 {
            return MonthDari::Hamal;
        } else if i == 2 {
            return MonthDari::Sur;
        } else if i == 3 {
            return MonthDari::Jauza;
        } else if i == 4 {
            return MonthDari::Saratan;
        } else if i == 5 {
            return MonthDari::Asad;
        } else if i == 6 {
            return MonthDari::Sonboleh;
        } else if i == 7 {
            return MonthDari::Mizan;
        } else if i == 8 {
            return MonthDari::Aqrab;
        } else if i == 9 {
            return MonthDari::Qos;
        } else if i == 10 {
            return MonthDari::Jady;
        } else if i == 11 {
            return MonthDari::Dolv;
        } else if i == 12 {
            return MonthDari::Hut;
        } else {
            panic!("invalid input");
        }
    }
}
// A Weekday specifies a day of the week starting from Shanbe = 0.
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone, Copy, ToString)]
pub enum WeekDay {
    Shanbeh = 0,
    Yekshanbeh,
    Doshanbeh,
    Seshanbeh,
    Charshanbeh,
    Panjshanbeh,
    Jomeh,
}
impl WeekDay {
    pub fn farsi_string(&self) -> String {
        let r = match self {
            WeekDay::Shanbeh => WEEK_DAY_FARSI[0],
            WeekDay::Yekshanbeh => WEEK_DAY_FARSI[1],
            WeekDay::Doshanbeh => WEEK_DAY_FARSI[2],
            WeekDay::Seshanbeh => WEEK_DAY_FARSI[3],
            WeekDay::Charshanbeh => WEEK_DAY_FARSI[4],
            WeekDay::Panjshanbeh => WEEK_DAY_FARSI[5],
            WeekDay::Jomeh => WEEK_DAY_FARSI[6],
        };
        return r.to_owned();
    }
    pub fn farsi_short_string(&self) -> String {
        let r = match self {
            WeekDay::Shanbeh => WEEK_DAY_FARSI_SHORT[0],
            WeekDay::Yekshanbeh => WEEK_DAY_FARSI_SHORT[1],
            WeekDay::Doshanbeh => WEEK_DAY_FARSI_SHORT[2],
            WeekDay::Seshanbeh => WEEK_DAY_FARSI_SHORT[3],
            WeekDay::Charshanbeh => WEEK_DAY_FARSI_SHORT[4],
            WeekDay::Panjshanbeh => WEEK_DAY_FARSI_SHORT[5],
            WeekDay::Jomeh => WEEK_DAY_FARSI_SHORT[6],
        };
        return r.to_owned();
    }
}
// A AmPm specifies the 12-Hour marker.
#[derive(Debug, Eq, PartialEq, Ord, PartialOrd, Clone, Copy, ToString)]
pub enum AmPm {
    Am,
    Pm,
}
impl AmPm {
    pub fn farsi_string(&self) -> String {
        let r = match self {
            AmPm::Am => AM_PM_FARSI[0],
            AmPm::Pm => AM_PM_FARSI[1],
        };
        r.to_owned()
    }

    pub fn farsi_short_string(&self) -> String {
        let r = match self {
            AmPm::Am => AM_PM_FARSI_SHORT[0],
            AmPm::Pm => AM_PM_FARSI_SHORT[1],
        };
        r.to_owned()
    }
}
pub const AM_PM_FARSI: [&'static str; 2] = ["قبل از ظهر", "بعد از ظهر"];
pub const AM_PM_FARSI_SHORT: [&'static str; 2] = ["ق.ظ", "ب.ظ"];
pub const MONTH_FARSI: [&'static str; 12] = [
    "فروردین",
    "اردیبهشت",
    "خرداد",
    "تیر",
    "مرداد",
    "شهریور",
    "مهر",
    "آبان",
    "آذر",
    "دی",
    "بهمن",
    "اسفند",
];
pub const MONTH_DARI: [&'static str; 12] = [
    "حمل",
    "ثور",
    "جوزا",
    "سرطان",
    "اسد",
    "سنبله",
    "میزان",
    "عقرب",
    "قوس",
    "جدی",
    "دلو",
    "حوت",
];
pub const WEEK_DAY_FARSI: [&'static str; 7] = [
    "شنبه",
    "یک‌شنبه",
    "دوشنبه",
    "سه‌شنبه",
    "چهارشنبه",
    "پنج‌شنبه",
    "جمعه",
];
pub const WEEK_DAY_FARSI_SHORT: [&'static str; 7] = ["ش", "ی", "د", "س", "چ", "پ", "ج"];
//  {days, leap_days, days_before_start}
pub const MONTH_COUNT: [[i32; 3]; 12] = [
    [31, 31, 0],   // Farvardin
    [31, 31, 31],  // Ordibehesht
    [31, 31, 62],  // Khordad
    [31, 31, 93],  // Tir
    [31, 31, 124], // Mordad
    [31, 31, 155], // Shahrivar
    [30, 30, 186], // Mehr
    [30, 30, 216], // Aban
    [30, 30, 246], // Azar
    [30, 30, 276], // Dey
    [30, 30, 306], // Bahman
    [29, 30, 336], // Esfand
];
// A Time represents a moment in time in Persian (Jalali) Calendar.
#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub struct Time {
    year: i32,
    month: Month,
    day: i32,
    hour: i32,
    min: i32,
    sec: i32,
    nsec: i32,
    wday: WeekDay,
}

impl Time {
    // new converts Gregorian calendar to Persian calendar and
    //
    // returns a new instance of Time corresponding to the time of t.
    //
    // t is an instance of time.Time in Gregorian calendar.
    pub fn new(dl: DateTime<Local>) -> Time {
        let mut time = Time {
            year: 0,
            month: Month::Farvardin,
            day: 0,
            hour: 0,
            min: 0,
            sec: 0,
            nsec: 0,
            wday: WeekDay::Seshanbeh,
        };
        time.set_time(dl);
        return time;
    }

    // now returns a new instance of Time corresponding to the current time.
    pub fn now() -> Time {
        return Time::new(chrono::Local::now());
    }

    // set sets t.
    //
    // year, month and day represent a day in Persian calendar.
    //
    // hour, min minute, sec seconds, nsec nanoseconds offsets represent a moment in time.
    pub fn set(
        &mut self,
        year: i32,
        month: Month,
        day: i32,
        hour: i32,
        min: i32,
        sec: i32,
        nsec: i32,
    ) {
        self.year = year;
        self.month = month;
        self.day = day;
        self.hour = hour;
        self.min = min;
        self.sec = sec;
        self.nsec = nsec;
        self.check_values_time();
        self.reset_weekday();
    }
    // date_time returns a new instance of Time.
    //
    // year, month and day represent a day in Persian calendar.
    //
    // hour, min minute, sec seconds, nsec nanoseconds offsets represent a moment in time.
    //
    // loc is a pointer to time.Location and must not be nil.
    pub fn date_time(
        year: i32,
        month: Month,
        day: i32,
        hour: i32,
        min: i32,
        sec: i32,
        nsec: i32,
    ) -> Time {
        let mut t = Time::new(Local::now());
        t.set(year, month, day, hour, min, sec, nsec);
        return t;
    }
    // set_year sets the year of t.
    pub fn set_year(&mut self, year: i32) {
        self.year = year;
        self.check_value_day();
        self.reset_weekday();
    }

    // set_month sets the month of t.
    pub fn set_month(&mut self, month: Month) {
        self.month = month;
        self.check_value_month();
        self.check_value_day();
        self.reset_weekday();
    }

    // set_day sets the day of t.
    pub fn set_day(&mut self, day: i32) {
        self.day = day;
        self.check_value_day();
        self.reset_weekday();
    }

    // set_hour sets the hour of t.
    pub fn set_hour(&mut self, hour: i32) {
        self.hour = hour;
        self.check_value_hour();
    }

    // set_minute sets the minute offset of t.
    pub fn set_minute(&mut self, min: i32) {
        self.min = min;
        self.check_value_minute();
    }

    // set_second sets the second offset of t.
    pub fn set_second(&mut self, sec: i32) {
        self.sec = sec;
        self.check_value_second();
    }

    // set_nano_second sets the nanosecond offset of t.
    pub fn set_nano_second(&mut self, nsec: i32) {
        self.nsec = nsec;
        self.check_value_nanosecond();
    }

    pub fn reset_weekday(&mut self) {
        self.wday = en_to_per_week_day(self.to_gregorian().weekday());
    }

    // is_leap returns true if the year of t is a leap year.
    pub fn is_leap(&self) -> bool {
        return divider(25 * self.year + 11, 33) < 8;
    }

    // to_gregorian converts Persian date to Gregorian date and returns a new instance of Time
    pub fn to_gregorian(&self) -> DateTime<chrono::offset::Local> {
        let year;
        let month;
        let day;
        let jdn = get_jdn(self.year, self.month as i32, self.day);

        if jdn > 2299160 {
            let mut l = jdn + 68569;
            let n = 4 * l / 146097;
            l = l - (146097 * n + 3) / 4;
            let i = 4000 * (l + 1) / 1461001;
            l = l - 1461 * i / 4 + 31;
            let j = 80 * l / 2447;
            day = l - 2447 * j / 80;
            l = j / 11;
            month = j + 2 - 12 * l;
            year = 100 * (n - 49) + i + l;
        } else {
            let mut j = jdn + 1402;
            let k = (j - 1) / 1461;
            let l = j - 1461 * k;
            let n = (l - 1) / 365 - l / 1461;
            let mut i = l - 365 * n + 30;
            j = 80 * i / 2447;
            day = i - 2447 * j / 80;
            i = j / 11;
            month = j + 2 - 12 * i;
            year = 4 * k + n + i - 4716;
        }
        return Local.ymd(year, month as u32, day as u32).and_hms_nano(
            self.hour as u32,
            self.min as u32,
            self.sec as u32,
            self.nsec as u32,
        );
    }

    // set_time sets t to the time of ti.
    pub fn set_time(&mut self, ti: DateTime<chrono::offset::Local>) {
        let mut year;
        let month;
        let day;

        self.nsec = ti.nanosecond() as i32;
        self.sec = ti.second() as i32;
        self.min = ti.minute() as i32;
        self.hour = ti.hour() as i32;
        self.wday = en_to_per_week_day(ti.weekday());
        let jdn;
        let gy = ti.year() as i32;
        let gmm = ti.month() as i32;
        let gd = ti.day() as i32;
        let gm = gmm;

        if gy > 1582 || (gy == 1582 && gm > 10) || (gy == 1582 && gm == 10 && gd > 14) {
            jdn = ((1461 * (gy + 4800 + ((gm - 14) / 12))) / 4)
                + ((367 * (gm - 2 - 12 * ((gm - 14) / 12))) / 12)
                - ((3 * ((gy + 4900 + ((gm - 14) / 12)) / 100)) / 4)
                + gd
                - 32075
        } else {
            jdn = 367 * gy - ((7 * (gy + 5001 + ((gm - 9) / 7))) / 4)
                + ((275 * gm) / 9)
                + gd
                + 1729777
        }

        let dep = jdn - get_jdn(475, 1, 1);
        let cyc = dep / 1029983;
        let rem = dep % 1029983;

        let ycyc;
        if rem == 1029982 {
            ycyc = 2820;
        } else {
            let a = rem / 366;
            ycyc = (2134 * a + 2816 * (rem % 366) + 2815) / 1028522 + a + 1;
        }

        year = ycyc + 2820 * cyc + 474;
        if year <= 0 {
            year = year - 1
        }

        let dy = (jdn - get_jdn(year, 1, 1) + 1) as f64;
        if dy <= 186.0 {
            month = ((dy / 31.0).ceil()) as i32;
        } else {
            month = (((dy - 6.0) / 30.0).ceil()) as i32;
        }

        day = jdn - get_jdn(year, month, 1) + 1;

        self.year = year;
        self.month = Month::month_number(month);
        self.day = day;
    }

    // at sets the hour, min minute, sec second and nsec nanoseconds offsets of t.
    pub fn at(&mut self, hour: i32, min: i32, sec: i32, nsec: i32) {
        self.set_hour(hour);
        self.set_minute(min);
        self.set_second(sec);
        self.set_second(nsec);
    }

    // unix returns the number of seconds since January 1, 1970 UTC.
    pub fn unix(&self) -> i64 {
        return self.to_gregorian().timestamp();
    }

    // unix_nano seturns the number of nanoseconds since January 1, 1970 UTC.
    pub fn unix_nano(&self) -> i64 {
        return self.to_gregorian().timestamp_nanos();
    }

    // date returns the year, month, day of t.
    pub fn date(&self) -> (i32, Month, i32) {
        return (self.year, self.month, self.day);
    }

    // clock returns the hour, minute, seconds offsets of t.
    pub fn clock(&self) -> (i32, i32, i32) {
        return (self.hour, self.min, self.sec);
    }

    // year returns the year of t.
    pub fn year(&self) -> i32 {
        return self.year;
    }

    // month returns the month of t in the range [1, 12].
    pub fn month(&self) -> Month {
        return self.month;
    }

    // month_dari returns the month of t in the range [1, 12].
    pub fn month_dari(&self) -> MonthDari {
        return MonthDari::month_number(self.month() as i32);
    }

    // day returns the day of month of t.
    pub fn day(&self) -> i32 {
        return self.day;
    }

    // hour returns the hour of t in the range [0, 23].
    pub fn hour(&self) -> i32 {
        return self.hour;
    }

    // hour12 returns the hour of t in the range [0, 11].
    pub fn hour12(&self) -> i32 {
        let mut h = self.hour;
        if h >= 12 {
            h -= 12
        }
        return h;
    }

    // minute returns the minute offset of t in the range [0, 59].
    pub fn minute(&self) -> i32 {
        return self.min;
    }

    // second returns the seconds offset of t in the range [0, 59].
    pub fn second(&self) -> i32 {
        return self.sec;
    }

    // nano_second returns the nanoseconds offset of t in the range [0, 999999999].
    pub fn nano_second(&self) -> i32 {
        return self.nsec;
    }

    // since returns the number of seconds between t and t2.
    pub fn since(&self, t2: Time) -> i64 {
        return (((t2.unix() - self.unix()) as f64).abs()) as i64;
    }

    // am_pm returns the 12-Hour marker of t.
    pub fn am_pm(&self) -> AmPm {
        let mut m = AmPm::Am;
        if self.hour > 12 || (self.hour == 12 && (self.min > 0 || self.sec > 0)) {
            m = AmPm::Pm;
        }
        return m;
    }

    // week_day returns the weekday of t.
    pub fn week_day(&self) -> WeekDay {
        return self.wday;
    }

    // remain_year_day returns the day of year of t.
    pub fn year_day(&self) -> i32 {
        return MONTH_COUNT[(self.month) as usize - 1][2] + self.day;
    }

    // remain_year_day returns the number of remaining days of the year of t.
    pub fn remain_year_day(&self) -> i32 {
        let mut y = 365;
        if self.is_leap() {
            y += 1;
        }
        return y - self.year_day();
    }

    // remain_month_day returns the number of remaining days of the month of t.
    pub fn remain_month_day(&self) -> i32 {
        let mut i = 0;
        if self.is_leap() {
            i = 1;
        }
        return MONTH_COUNT[(self.month as usize) - 1][i] - self.day;
    }

    // first_weekday returns a new instance of Time representing the first day of the week of t.
    pub fn first_weekday(&self) -> Time {
        if self.wday == WeekDay::Shanbeh {
            return self.clone();
        }

        return self.add(chrono::Duration::days(
            ((WeekDay::Shanbeh as i32) - (self.wday as i32)) as i64,
        ));
    }

    // last_weekday returns a new instance of Time representing the last day of the week of t.
    pub fn last_weekday(&self) -> Time {
        if self.wday == WeekDay::Jomeh {
            return self.clone();
        }
        return self.add(chrono::Duration::days(
            ((WeekDay::Jomeh as i32) - (self.wday as i32)) as i64,
        ));
    }

    // first_monthday returns a new instance of Time representing the first day of the month of t.
    pub fn first_monthday(&self) -> Time {
        if self.day == 1 {
            return self.clone();
        }
        return Time::date_time(
            self.year, self.month, 1, self.hour, self.min, self.sec, self.nsec,
        );
    }

    // last_monthday returns a new instance of Time representing the last day of the month of t.
    pub fn last_monthday(&self) -> Time {
        let mut i = 0;
        if self.is_leap() {
            i = 1;
        }

        let ld = MONTH_COUNT[((self.month as i32) - 1) as usize][i];

        if ld == self.day {
            return self.clone();
        }

        return Time::date_time(
            self.year, self.month, ld, self.hour, self.min, self.sec, self.nsec,
        );
    }

    // first_yearday returns a new instance of Time representing the first day of the year of t.
    pub fn first_yearday(&self) -> Time {
        if self.month == Month::Farvardin && self.day == 1 {
            return self.clone();
        }
        return Time::date_time(
            self.year,
            Month::Farvardin,
            1,
            self.hour,
            self.min,
            self.sec,
            self.nsec,
        );
    }

    // last_yearday returns a new instance of Time representing the last day of the year of t.
    pub fn last_yearday(&self) -> Time {
        let mut i = 0;
        if self.is_leap() {
            i = 1;
        }
        let ld = MONTH_COUNT[(Month::Esfand as usize) - 1][i];
        if self.month == Month::Esfand && self.day == ld {
            return self.clone();
        }
        return Time::date_time(
            self.year,
            Month::Esfand,
            ld,
            self.hour,
            self.min,
            self.sec,
            self.nsec,
        );
    }

    // month_week returns the week of month of t.
    pub fn month_week(&self) -> i32 {
        return (((self.day as f64) + ((self.first_monthday().week_day() as i32) as f64) / 7.0)
            .ceil()) as i32;
    }

    // year_week returns the week of year of t.
    pub fn year_week(&self) -> i32 {
        return (((self.year_day() + (self.first_yearday().week_day()) as i32) as f64) / 7.0).ceil()
            as i32;
    }

    // remain_year_week returns the number of remaining weeks of the year of t.
    pub fn remain_year_week(&self) -> i32 {
        return 52 - self.year_week();
    }

    // yesterday returns a new instance of Time representing a day before the day of t.
    pub fn yesterday(&self) -> Time {
        let du = chrono::Duration::days(1);
        return self.add(-du);
    }

    // tomorrow returns a new instance of Time representing a day after the day of t.
    pub fn tomorrow(&self) -> Time {
        let du = chrono::Duration::days(1);
        return self.add(du);
    }
    // add returns a new instance of Time for t+d.
    pub fn add(&self, d: chrono::Duration) -> Time {
        let g_date = self.to_gregorian() + d;
        return Time::new(g_date);
    }

    // Add_date returns a new instance of Time for t.year+years, t.month+months and t.day+days.
    pub fn add_date(&self, years: i32, months: i32, days: i32) -> Time {
        let da = ((years * 336) + (months * 30) + days) as i64;
        let du = chrono::Duration::days(da); // TODO
        return self.add(du);
    }

    fn check_value_nanosecond(&mut self) {
        let max = 999999999;
        let min = 0;
        if self.nsec < min && self.nsec > max {
            panic!("nanosecond value invalid !");
        }
    }

    fn check_value_second(&mut self) {
        let max = 59;
        let min = 0;
        if self.sec < min && self.sec > max {
            panic!("second value invalid !");
        }
    }

    fn check_value_minute(&mut self) {
        let max = 59;
        let min = 0;
        if self.min < min && self.min > max {
            panic!("minute value invalid !");
        }
    }

    fn check_value_hour(&mut self) {
        let max = 23;
        let min = 0;
        if self.hour < min && self.hour > max {
            panic!("hour value invalid !");
        }
    }

    fn check_value_month(&mut self) {
        let max = Month::Farvardin;
        let min = Month::Esfand;
        if self.month < min && self.month > max {
            panic!("month value invalid !");
        }
    }

    fn check_value_day(&mut self) {
        let mut i = 0;
        if self.is_leap() {
            i = 1
        }
        let max = MONTH_COUNT[((self.month as usize) - 1)][i];
        let min = 1;
        if self.day < min && self.day > max {
            panic!("day value invalid !");
        }
    }

    fn check_values_time(&mut self) {
        self.check_value_nanosecond();
        self.check_value_second();
        self.check_value_minute();
        self.check_value_hour();
        self.check_value_month();
        self.check_value_day();
    }
    // FIX ME
    pub fn format(&self, format: &str) -> String {
        match format {
            // year
            "yyyy" => self.year().to_string(),
            // year
            "yyy" => self.year().to_string(),
            // 2-digits representation of year
            "yy" => format!("{:02}", self.year()),
            // year
            "y" => self.year().to_string(),
            // MMM the Persian name of month (e.g. فروردین)
            "MMM" => self.month().farsi_string(),
            // MMI the Dari name of month (e.g. حمل)
            "MMI" => self.month_dari().farsi_string(),
            // MM 2-digits representation of month (e.g. 01)
            "MM" => format!("{:02}", self.month() as i32),
            // M month (e.g. 1)
            "M" => (self.month() as i32).to_string(),
            // rw remaining weeks of year
            "rw" => self.remain_year_week().to_string(),
            // w week of year
            "w" => self.year_week().to_string(),
            // RW remaining weeks of month
            // "RW" => self.remain_month_week().to_string(),
            // W week of month
            "W" => self.month_week().to_string(),
            // RD remaining days of year
            "RD" => self.remain_year_day().to_string(),
            // D day of year
            "D" => self.year_day().to_string(),
            // rd remaining days of month
            "rd" => self.remain_month_day().to_string(),
            // dd 2-digits representation of day (e.g. 01)
            "dd" => format!("{:02}", self.day()), // FIX BUG
            // d day (e.g. 1)
            "d" => self.day().to_string(),
            // E the Persian name of weekday (e.g. شنبه)
            "E" => self.week_day().farsi_string(),
            // e the Persian short name of weekday (e.g. ش)
            "e" => self.week_day().farsi_short_string(),
            // A the Persian name of 12-Hour marker (e.g. قبل از ظهر)
            "A" => self.am_pm().farsi_string(),
            // a the Persian short name of 12-Hour marker (e.g. ق.ظ)
            "a" => self.am_pm().farsi_short_string(),
            // HH 2-digits representation of hour [00-23]
            "HH" => format!("{:02}", self.hour()),
            // H hour [0-23]
            "H" => self.hour().to_string(),
            // kk 2-digits representation of hour [01-24]
            "KK" => format!("{:02}", modify_hour(self.hour(), 24)),
            // k hour [1-24]
            "K" => self.hour().to_string(),
            // hh 2-digits representation of hour [01-12]
            "hh" => format!("{:02}", self.hour12()),
            //h hour [1-12]
            "h" => self.hour12().to_string(),
            // KK 2-digits representation of hour [00-11]
            "kk" => format!("{:02}", self.hour12()),
            // K hour [0-11]
            "k" => self.hour12().to_string(),
            // mm 2-digits representation of minute [00-59]
            "mm" => format!("{:02}", self.minute()),
            // m minute [0-59]
            "m" => self.minute().to_string(),
            // ss 2-digits representation of seconds [00-59]
            "ss" => format!("{:02}", self.second()),
            // ns nanoseconds
            "ns" => self.nano_second().to_string(),
            // s seconds [0-59]
            "s" => self.second().to_string(),
            // S 3-digits representation of milliseconds (e.g. 001)
            "S" => format!("{:03}", self.nano_second()),
            _ => panic!("invalid string format !"),
        }
    }
}

fn get_jdn(year: i32, month: i32, day: i32) -> i32 {
    let mut base = year - 473;
    if year >= 0 {
        base -= 1;
    }
    let epy = 474 + (base % 2820);

    let md: i32;
    if month <= 7 {
        md = (month - 1) * 31;
    } else {
        md = (month - 1) * 30 + 6;
    }

    return day + md + (epy * 682 - 110) / 2816 + (epy - 1) * 365 + base / 2820 * 1029983 + 1948320;
}

pub fn en_to_per_week_day(wd: chrono::Weekday) -> WeekDay {
    use chrono::Weekday as EnWeekDay;
    match wd {
        EnWeekDay::Sat => return WeekDay::Shanbeh,
        EnWeekDay::Sun => return WeekDay::Yekshanbeh,
        EnWeekDay::Mon => return WeekDay::Doshanbeh,
        EnWeekDay::Tue => return WeekDay::Seshanbeh,
        EnWeekDay::Wed => return WeekDay::Charshanbeh,
        EnWeekDay::Thu => return WeekDay::Panjshanbeh,
        EnWeekDay::Fri => return WeekDay::Jomeh,
    }
}

fn is_persian_leap(year: i32) -> bool {
    return divider(25 * year + 11, 33) < 8;
}

fn is_gregorian_leap(year: i32) -> bool {
    return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
}

fn divider(num: i32, den: i32) -> i32 {
    if num > 0 {
        return num % den;
    } else {
        return num - ((((num + 1) / den) - 1) * den);
    }
}

fn modify_hour(val: i32, max: i32) -> i32 {
    if val == 0 {
        return max;
    }
    return val;
}
